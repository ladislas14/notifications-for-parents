var express = require('express'),
    app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server)
  , cons = require('consolidate');

var j5 = require("johnny-five");
var board = new j5.Board();
var LEDPIN = 12;
var BTNPIN = 2;
var ledOn = false;

app.engine('html', cons.mustache);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.use(express.static(__dirname + '/public'));

server.listen(8080);

app.get('/', function(req, res) {
    res.render('index');
});
app.get('/client', function(req, res) {
    res.render('client');
});

io.sockets.on('connection', function(socket){
    socket.emit('connected');
});

board.on("ready", function(){
    var led = new j5.Led(LEDPIN);
    var btn = new j5.Button(BTNPIN);

    btn.on("hit", function(){
        led.on();
        console.log("bouton appuyé");
        io.sockets.emit('notif');
    });


    btn.on("release", function(){
        led.off();
    });

});
